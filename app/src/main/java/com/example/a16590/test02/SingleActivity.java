package com.example.a16590.test02;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.other_utils.Bean.DeviceInfo;
import com.other_utils.Bean.RxPageStatusData;
import com.other_utils.BleByteUtils;
import com.other_utils.MgrUtils.Constant;
import com.other_utils.MgrUtils.StrByteHex;
import com.other_utils.MgrUtils.Uitils;
import com.other_utils.utils_lib.ConvertUtils;
import com.alibaba.fastjson.JSON;
import com.ble_lib.BleAdmin;
import com.ble_lib.BleDeviceService;
import com.ble_lib.SampleGattAttributes;
import com.ble_lib.callback.DeviceConnectStateCallback;
import com.ble_lib.callback.DeviceOperationCallback;
import com.ble_lib.callback.ScanCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class SingleActivity extends AppCompatActivity implements DeviceConnectStateCallback, DeviceOperationCallback, View.OnClickListener {
    private static final String TAG = "SingleActivity";
    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    private TextView tv1;
    private BleAdmin bleAdmin;
    private BleByteUtils bleByteUtils;//该工具类负责管理各种向设备发送的指令
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics;//服务-特征列表
    private DeviceInfo currentDevice;
    private BluetoothGattCharacteristic mNotifyCharacteristic;//接收消息的特征
    private BluetoothGattCharacteristic finalCharacteristicWrite;//写数据的特征
    private UUID writeUuid;
    private String deviceAddress = "18:7A:93:06:07:01";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single);

        tv1 = (TextView) findViewById(R.id.tv1);
        findViewById(R.id.tv1).setOnClickListener(this);
        findViewById(R.id.btn1).setOnClickListener(this);
        findViewById(R.id.btn2).setOnClickListener(this);
        findViewById(R.id.btn3).setOnClickListener(this);
        findViewById(R.id.btn4).setOnClickListener(this);

        bleAdmin = new BleAdmin(this, this, this);
        bleByteUtils = new BleByteUtils();
        currentDevice = new DeviceInfo();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv1:
                tv1.setText("");
                Toast.makeText(this, "清屏成功", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn1:
                //启动连接根据蓝牙mac地址
                bleAdmin.connectDevice(deviceAddress);
                break;
            case R.id.btn2:
                //启动广播接收
                List<ScanFilter> scanFilterss = new ArrayList<>();
                scanFilterss = Uitils.scanFilters();
                bleAdmin.startFilterScan(scanFilterss, Uitils.initScanSettings(), new ScanCallback() {
                    @Override
                    public void onDeviceFound(BluetoothDevice devices, ScanRecord scanRecord) {
                        final String json = JSON.toJSONString(BleByteUtils.scanRecordToData(scanRecord));
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv1.append("\n" + json);
                            }
                        });
                    }

                    @Override
                    public void onFoundFail() {
                    }
                });
                break;
            case R.id.btn3:
                bleAdmin.processDeviceService(new BleDeviceService(
                        deviceAddress,
                        writeUuid,
                        BleDeviceService.OperateType.Write,
                        bleByteUtils.requestRealTimeData()//请求实时数据
                ));
                break;
            case R.id.btn4:
                bleAdmin.processDeviceService(new BleDeviceService(
                        deviceAddress,
                        writeUuid,
                        BleDeviceService.OperateType.Write,
                        bleByteUtils.setDoubleDeviceAlarm(0, 66, 22)//设置报警温度 type:0温度，1湿度
                ));

                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {//检测蓝牙开关是否开启
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 1);
        }
    }

    @Override
    public void onDeviceConnected(String address) {
        Log.v(TAG, "onDeviceConnected-address=" + address);
        bleAdmin.discoverDeviceServices(address);//启动发现服务
    }

    @Override
    public void onDeviceDisconnected() {
        Log.v(TAG, "onDeviceDisconnected");
    }

    @Override
    public void onDeviceServiceDiscover(final String deviceAddress, List<BluetoothGattService> services, BluetoothGatt gatt) {
        Log.v(TAG, "onDeviceServiceDiscover-deviceAddress=" + deviceAddress);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tv1.setText("连接" + deviceAddress + "成功");
            }
        });
        if (services == null) return;
        String uuid = null;
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<>();
        mGattCharacteristics = new ArrayList<>();
        for (BluetoothGattService gattService : services) {
            HashMap<String, String> currentServiceData = new HashMap<>();
            uuid = gattService.getUuid().toString();
            String serviceName = SampleGattAttributes.lookup(uuid);
            if (!TextUtils.isEmpty(serviceName)) {
                currentServiceData.put(LIST_NAME, serviceName);
                currentServiceData.put(LIST_UUID, uuid);
                gattServiceData.add(currentServiceData);
            } else {
                continue;
            }
            ArrayList<HashMap<String, String>> gattCharacteristicGroupData = new ArrayList<>();
            List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas = new ArrayList<>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                String name = SampleGattAttributes.lookup(uuid);
                if (!TextUtils.isEmpty(name)) {
                    currentCharaData.put(LIST_NAME, name);
                    currentCharaData.put(LIST_UUID, uuid);
                    gattCharacteristicGroupData.add(currentCharaData);
                } else {
                    continue;
                }
            }
            mGattCharacteristics.add(charas);
        }
        ////////////////////////////////////////////////////////////////
        BluetoothGattCharacteristic characteristicMotify = null;
        BluetoothGattCharacteristic characteristicWrite = null;
        for (int i = 0; i < mGattCharacteristics.size(); i++) {
            ArrayList<BluetoothGattCharacteristic> arrayList = mGattCharacteristics.get(i);
            for (int j = 0; j < arrayList.size(); j++) {
                if (arrayList.get(j).getUuid().toString().equals(SampleGattAttributes.UUID_NOTIFY)) {
                    characteristicMotify = arrayList.get(j);
                }
                if (arrayList.get(j).getUuid().toString().equals(SampleGattAttributes.UUID_WRITE)) {
                    characteristicWrite = arrayList.get(j);
                }
            }
        }
        if (characteristicMotify != null) {
            int charaProp = characteristicMotify.getProperties();
            if ((charaProp & BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                //如果当前有一个NOTIFY的特征正在活动，要先关闭NOTIFY特征，再去开启READ特征
                //                                setCharacteristicNotification(mNotifyCharacteristic, false);
                characteristicMotify = null;
                //开启读特征
                bleAdmin.processDeviceService(new BleDeviceService(currentDevice.getId(),
                        characteristicMotify.getUuid(),
                        BleDeviceService.OperateType.Read));
//                            readCharacteristic(characteristic);
            }
            if ((charaProp & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                mNotifyCharacteristic = characteristicMotify;
                bleAdmin.processDeviceService(new BleDeviceService(
                        currentDevice.getId(),
                        characteristicMotify.getUuid(),
                        BleDeviceService.OperateType.Notify));
            }

            finalCharacteristicWrite = characteristicWrite;
            int charaProp2 = 0;
            if (finalCharacteristicWrite != null) {
                charaProp2 = finalCharacteristicWrite.getProperties();
                if ((charaProp2 & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0) {
                    bleAdmin.processDeviceService(new BleDeviceService(
                            currentDevice.getId(),
                            finalCharacteristicWrite.getUuid(),
                            BleDeviceService.OperateType.Write,
                            bleByteUtils.breakOffHistory()//停止历史数据获取
                    ));
                }
            }
            writeUuid = finalCharacteristicWrite.getUuid();
        }
    }

    @Override
    public void onDeviceCharacteristicRead(String deviceAddress, BluetoothGattCharacteristic characteristic) {
        Log.v(TAG, "onDeviceCharacteristicRead=" + StrByteHex.bytesToHexString(characteristic.getValue()));
    }

    @Override
    public void onDeviceCharacteristicWrite(String deviceAddress) {
        Log.v(TAG, "onDeviceCharacteristicWrite-deviceAddress=" + deviceAddress);
    }

    @Override
    public void onDeviceCharacteristicNotify(String deviceAddress, BluetoothGattCharacteristic characteristic) {
        int status = 0;
        final byte[] datas = characteristic.getValue();
        Log.v(TAG, "onDeviceCharacteristicNotify-datas=" + StrByteHex.bytesToHexString(datas));
        if (datas != null && datas.length > 0) {
            bleByteUtils.logPrint(datas);
            switch (status) {
                case RxPageStatusData.GET_REATAIL_TIME_DATA:
                    if (bleByteUtils.check(datas)) Log.e(TAG, "首页授时成功");
                    else Log.e(TAG, "首页授时失败");
                    break;
                case RxPageStatusData.UPDATE_DEVICE_ID:
                    bindDeviceStatus(datas);
                    break;
                case RxPageStatusData.GET_HISTORY:
//                    getHistoryData(datas);
                    break;
                case RxPageStatusData.SET_ALARM_VALUE:
//                    setAlarmData(datas);
                    break;
                case RxPageStatusData.SET_DEVICE_TIME:
//                    setDeviceTimeSuccess();
                    break;
            }
        }
    }

    @Override
    public void CharacteristicWriteFail() {
    }

    //数据修改函数
    private void bindDeviceStatus(byte[] data) {
        if (bleByteUtils.check(data)) {
            if (data[4] == Constant.login_id_is) {
                byte[] types = new byte[2];
                types[0] = data[8];
                types[1] = data[7];
                String strChar = ConvertUtils.bytes2Bits(types);
//                listener.bindStepOneSuccess();
                Log.v(TAG, "首页授时失败");
            } else {
//                listener.bindStepTwoSuccess(ConvertUtils.swapWords(strChar));
//                strChar = null;
            }
        } else {
//            listener.fail();
        }
    }
}
