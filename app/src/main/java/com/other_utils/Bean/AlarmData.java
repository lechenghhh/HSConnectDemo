package com.other_utils.Bean;


/**
 * Created by 90835 on 2017/10/26.
 */

public class AlarmData {

    private String macId;
    private int maxTemperatureAlarmValue;
    private int minhumidityAlarmValue;
    private int maxhumidityAlarmValue;
    private int minTemperatureAlarmValue;
    private int AlarmType; //0:温度，否则或者1为湿度;
    private boolean isOpen;
    private String place;
    private String deviceName;
    private int temperatureCount;
    private int humidityCount;
    private String temperatureMaxDescription;
    private String temperatureMinDescription;
    private String humidityMaxDescription;
    private String humidityMinDescription;

    public AlarmData(String macId, int maxTemperatureAlarmValue,
                     int minhumidityAlarmValue, int maxhumidityAlarmValue,
                     int minTemperatureAlarmValue, int AlarmType, boolean isOpen,
                     String place, String deviceName, int temperatureCount,
                     int humidityCount, String temperatureMaxDescription,
                     String temperatureMinDescription, String humidityMaxDescription,
                     String humidityMinDescription) {
        this.macId = macId;
        this.maxTemperatureAlarmValue = maxTemperatureAlarmValue;
        this.minhumidityAlarmValue = minhumidityAlarmValue;
        this.maxhumidityAlarmValue = maxhumidityAlarmValue;
        this.minTemperatureAlarmValue = minTemperatureAlarmValue;
        this.AlarmType = AlarmType;
        this.isOpen = isOpen;
        this.place = place;
        this.deviceName = deviceName;
        this.temperatureCount = temperatureCount;
        this.humidityCount = humidityCount;
        this.temperatureMaxDescription = temperatureMaxDescription;
        this.temperatureMinDescription = temperatureMinDescription;
        this.humidityMaxDescription = humidityMaxDescription;
        this.humidityMinDescription = humidityMinDescription;
    }

    public AlarmData() {
    }

    public String getTemperatureMaxDescription() {
        return temperatureMaxDescription;
    }

    public void setTemperatureMaxDescription(String temperatureMaxDescription) {
        this.temperatureMaxDescription = temperatureMaxDescription;
    }

    public String getTemperatureMinDescription() {
        return temperatureMinDescription;
    }

    public void setTemperatureMinDescription(String temperatureMinDescription) {
        this.temperatureMinDescription = temperatureMinDescription;
    }

    public String getHumidityMaxDescription() {
        return humidityMaxDescription;
    }

    public void setHumidityMaxDescription(String humidityMaxDescription) {
        this.humidityMaxDescription = humidityMaxDescription;
    }

    public String getHumidityMinDescription() {
        return humidityMinDescription;
    }

    public void setHumidityMinDescription(String humidityMinDescription) {
        this.humidityMinDescription = humidityMinDescription;
    }

    public int getTemperatureCount() {
        return temperatureCount;
    }

    public void setTemperatureCount(int temperatureCount) {
        this.temperatureCount = temperatureCount;
    }

    public int getHumidityCount() {
        return humidityCount;
    }

    public void setHumidityCount(int humidityCount) {
        this.humidityCount = humidityCount;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getMacId() {
        return macId;
    }

    public void setMacId(String macId) {
        this.macId = macId;
    }

    public int getMaxTemperatureAlarmValue() {
        return maxTemperatureAlarmValue;
    }

    public void setMaxTemperatureAlarmValue(int maxTemperatureAlarmValue) {
        this.maxTemperatureAlarmValue = maxTemperatureAlarmValue;
    }

    public int getMinhumidityAlarmValue() {
        return minhumidityAlarmValue;
    }

    public void setMinhumidityAlarmValue(int minhumidityAlarmValue) {
        this.minhumidityAlarmValue = minhumidityAlarmValue;
    }

    public int getMaxhumidityAlarmValue() {
        return maxhumidityAlarmValue;
    }

    public void setMaxhumidityAlarmValue(int maxhumidityAlarmValue) {
        this.maxhumidityAlarmValue = maxhumidityAlarmValue;
    }

    public int getMinTemperatureAlarmValue() {
        return minTemperatureAlarmValue;
    }

    public void setMinTemperatureAlarmValue(int minTemperatureAlarmValue) {
        this.minTemperatureAlarmValue = minTemperatureAlarmValue;
    }

    public int getAlarmType() {
        return AlarmType;
    }

    public void setAlarmType(int alarmType) {
        AlarmType = alarmType;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public boolean getIsOpen() {
        return this.isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }
}
