package com.other_utils.Bean;

import android.support.annotation.NonNull;

/**
 * Created by 90835 on 2017/10/24.
 */

public class HistoryLastInfo implements Comparable<HistoryLastInfo> {

    private String id;
    private String macId;

    private int SerialNumber;
    //时区
    private int timeZone;
    //夏令时
    private int dst;
    //年
    private int year;
    //月
    private int month;
    //日
    private int day;

    private int hours;

    private int minute;

    private int second;

    private float TemperatureValue;

    private float HumidityValue;

    private float PressureValue;

    //0:为没有报警，1：为温度报警，2为湿度报警,3为两个都报警
    private int alarmStatus;

    private String time;
    private long longTime;

    private String place;
    private float maxValue;
    private float minValue;
    private boolean isSelect = false;


    public HistoryLastInfo(String id, String macId, int SerialNumber, int timeZone,
                           int dst, int year, int month, int day, int hours, int minute,
                           int second, float TemperatureValue, float HumidityValue,
                           float PressureValue, int alarmStatus, String time, long longTime,
                           String place) {
        this.id = id;
        this.macId = macId;
        this.SerialNumber = SerialNumber;
        this.timeZone = timeZone;
        this.dst = dst;
        this.year = year;
        this.month = month;
        this.day = day;
        this.hours = hours;
        this.minute = minute;
        this.second = second;
        this.TemperatureValue = TemperatureValue;
        this.HumidityValue = HumidityValue;
        this.PressureValue = PressureValue;
        this.alarmStatus = alarmStatus;
        this.time = time;
        this.longTime = longTime;
        this.place = place;
    }

    public HistoryLastInfo() {
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public long getLongTime() {
        return longTime;
    }

    public void setLongTime(long longTime) {
        this.longTime = longTime;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public float getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(float maxValue) {
        this.maxValue = maxValue;
    }

    public float getMinValue() {
        return minValue;
    }

    public void setMinValue(float minValue) {
        this.minValue = minValue;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getAlarmStatus() {
        return alarmStatus;
    }

    public void setAlarmStatus(int alarmStatus) {
        this.alarmStatus = alarmStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMacId() {
        return macId;
    }

    public void setMacId(String macId) {
        this.macId = macId;
    }

    public int getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        SerialNumber = serialNumber;
    }

    public float getTemperatureValue() {
        return TemperatureValue;
    }

    public void setTemperatureValue(float temperatureValue) {
        TemperatureValue = temperatureValue;
    }

    public float getHumidityValue() {
        return HumidityValue;
    }

    public void setHumidityValue(float humidityValue) {
        HumidityValue = humidityValue;
    }

    public float getPressureValue() {
        return PressureValue;
    }

    public void setPressureValue(float pressureValue) {
        PressureValue = pressureValue;
    }


    public int getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(int timeZone) {
        this.timeZone = timeZone;
    }

    public int getDst() {
        return dst;
    }

    public void setDst(int dst) {
        this.dst = dst;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }


    @Override
    public int compareTo(@NonNull HistoryLastInfo historyLastInfo) {
        if (this.getHours() > historyLastInfo.getHours()) {
            return 1;
        } else if (this.getHours() == historyLastInfo.getHours()) {
            if (this.getMinute() >=historyLastInfo.getMinute()) {
                return 1;
            } else {
                return -1;
            }
        }
        return -1;
    }
}
