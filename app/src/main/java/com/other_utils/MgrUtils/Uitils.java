package com.other_utils.MgrUtils;

import android.annotation.TargetApi;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanSettings;
import android.os.Build;
import android.os.ParcelUuid;

import java.util.ArrayList;
import java.util.List;

import static android.bluetooth.le.ScanSettings.MATCH_MODE_AGGRESSIVE;

/**
 * Created by 90835 on 2017/10/23.
 */

public class Uitils {

    public static List<ScanFilter> scanFilters() {
        ScanFilter.Builder builder = new ScanFilter.Builder()
                .setServiceUuid(ParcelUuid.fromString(Constant.mServiceUuids));
        List<ScanFilter> filters = new ArrayList<>();
        ScanFilter scanFilter = builder.build();
        filters.add(scanFilter);

        return filters;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static ScanSettings initScanSettings() {
        ScanSettings.Builder builder = new ScanSettings.Builder();
        builder.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY);
        try {
            builder.setMatchMode(MATCH_MODE_AGGRESSIVE);
        } catch (Error e) {
        }
        return builder.build();
    }
}
