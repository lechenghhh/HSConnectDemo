package com.other_utils.MgrUtils;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by 90835 on 2017/10/9.
 */

public class TimeZoneUtils {

    private static Calendar c = Calendar.getInstance();

    /*
    * 获取当前时区
    * @return
    * */
    public static int getCurrentTimeZone() {
        TimeZone tz = TimeZone.getDefault();
        int valueTz = tz.getRawOffset() / 60000 / 60 % 60;
//        String hours = TimeUtils.stringToString(strTz, TimeUtils.GMT_HOURS, TimeUtils.HOUR_PATTERN);
        return valueTz;
    }

    /*
    * 获取当前年
    * @return
    * */
    public static int getYear() {
        return c.get(Calendar.YEAR);
    }

    /*
 * 获取当前年
 * @return
 * */
    public static int getYear(long time) {
        c.setTimeInMillis(time);
        return c.get(Calendar.YEAR);
    }

    /*
* 获取当前月
* @return
* */
    public static int getMonth() {
        return c.get(Calendar.MONTH) + 1;
    }

    public static int getMonth(long time) {
        c.setTimeInMillis(time);
        return c.get(Calendar.MONTH) + 1;
    }

    /*
* 获取当前日
* @return
* */
    public static int getDay() {
        return c.get(Calendar.DAY_OF_MONTH);
    }

    public static int getDay(long time) {
        c.setTimeInMillis(time);
        return c.get(Calendar.DAY_OF_MONTH);
    }

    public static int getHours() {
        return c.get(Calendar.HOUR_OF_DAY);
    }

    public static int getHours(long time) {
        c.setTimeInMillis(time);
        return c.get(Calendar.HOUR_OF_DAY);
    }

    public static int getMinute() {
        return c.get(Calendar.MINUTE);
    }

    public static int getMinute(long time) {
        c.setTimeInMillis(time);
        return c.get(Calendar.MINUTE);
    }


}
