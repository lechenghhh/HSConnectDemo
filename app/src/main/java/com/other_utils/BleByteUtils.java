package com.other_utils;

import android.bluetooth.le.ScanRecord;
import android.util.Log;

import com.other_utils.Bean.AlarmData;
import com.other_utils.Bean.RealTimeData;
import com.other_utils.MgrUtils.Constant;
import com.other_utils.MgrUtils.GetCharAscii;
import com.other_utils.Bean.HistoryLastInfo;
import com.other_utils.Bean.TimeData;
import com.other_utils.MgrUtils.TimeZoneUtils;
import com.other_utils.utils_lib.ConvertUtils;
import com.other_utils.utils_lib.LogUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
/*Created by 90835on 2017/4/25.
 * 该工具类负责管理各种向设备发送的指令
 */

public class BleByteUtils {
    final List<byte[]> dataArr = new ArrayList<>();
    private byte[] protoData;
    private ArrayList<Byte> datas;

    public static final String[] chars = new String[]{"a", "b", "c", "d", "e", "f",
            "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
            "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z"};


    public BleByteUtils() {


    }

    private void initData() {//每个指令之前都要调用
        datas = new ArrayList<Byte>();
        //头部
        datas.add((byte) Constant.HEADER_IS);
        //型号
        datas.add((byte) Constant.MODEL_IS);
        //编号
        datas.add((byte) Constant.CONTROL_NO);
    }

    //请求更新设备ID
    public byte[] packData() {
        initData();
        //指令
        datas.add((byte) (Constant.S_LEVEL_IS & 0xFF));
        datas.add((byte) (Constant.login_id_is & 0xFF));
        //应答
        datas.add((byte) Constant.REPLY);
        //数据长度两个字节
        datas.add((byte) (3 & 0xFF));
        //数据类型及长度
        datas.add((byte) ((12 & 0x1F) << 3 | (1 & 0x07)));
        //数据对应单位以及小数点位数
        datas.add((byte) ((10 & 0x1F) << 3 | (0 & 0x07)));
        //数据
        datas.add((byte) (generateShortUuid() & 0xFF));
        //校验和
        datas.add(getChecksum(datas));
        datas.add((byte) Constant.TAIL);
        return listTobyte(datas);
    }

    /*
     * 获取历史数据
     * */

    public byte[] packHistoryData(HistoryLastInfo historyLastInfo) {
        initData();
        //指令
        datas.add((byte) (Constant.V_SECONDARY_IS & 0xFF));
        datas.add((byte) (Constant.get_history_data & 0xFF));
        //应答
        datas.add((byte) Constant.REPLY);
        //数据长度两个字节
        datas.add((byte) 8);
        //数据1接收到最后地址
        datas.add((byte) 0);
        LogUtils.e("取前8位：", historyLastInfo.getSerialNumber() & 0xFF00 >> 8);
        datas.add((byte) (historyLastInfo.getSerialNumber() & 0xFF << 8));
        LogUtils.e("取后8位：", historyLastInfo.getSerialNumber() & 0xFF << 8);
        //时区
        datas.add(((byte) (TimeZoneUtils.getCurrentTimeZone() & 0xFF)));
        //年
        datas.add((byte) ((historyLastInfo.getYear() & 0xFF)));
        //月
        datas.add((byte) (historyLastInfo.getMonth() & 0xFF));
        //日
        datas.add((byte) (historyLastInfo.getDay() & 0xFF));
        //时
        datas.add((byte) (historyLastInfo.getHours() & 0xFF));
        //分
        datas.add((byte) (historyLastInfo.getMinute() & 0xFF));
        //校验和
        datas.add(getChecksum(datas));
        datas.add((byte) Constant.TAIL);
        return listTobyte(datas);
    }

    //中止设备数据
    public byte[] breakOffHistory() {
        initData();
        datas.add((byte) 0x43);
        datas.add((byte) 0x01);
        datas.add((byte) 0x1);
        datas.add((byte) 0x2);
        datas.add((byte) 0x1);
        datas.add((byte) 0x1);
        datas.add(getChecksum(datas));
        datas.add((byte) Constant.TAIL);
        return listTobyte(datas);
    }

    // 请求实时数据
    public byte[] requestRealTimeData() {
        initData();
        //指令
        datas.add((byte) (Constant.V_SECONDARY_IS & 0xFF));
        datas.add((byte) (Constant.device_up_data & 0xFF));
        //应答
        datas.add((byte) Constant.REPLY);
        //数据长度两个字节
        datas.add((byte) 0);
        //校验和
        datas.add(getChecksum(datas));
        datas.add((byte) Constant.TAIL);
        return listTobyte(datas);
    }

    //将ScanRecord转化成RealTimeData数据类型，内含实施参数
    public static RealTimeData scanRecordToData(ScanRecord scanRecord) {

        String str = scanRecord.getDeviceName();
        RealTimeData realTimeData = new RealTimeData();

        Log.v("RealTimeDataUtils", str);
        String[] names = str.split("0\\u002B0\\u002B");//根据0+0+拆分
        realTimeData.setDeviceName(names[0]);
        try {
            int length = 8;
            String tempStr = str.substring(length, length + 4);
            length += tempStr.length();
            String humdityStr = str.substring(length, length + 2);
            length += humdityStr.length();
            String pressureStr = str.substring(length, length + 4);
            //解析温度

            int tempOneChart = tempStr.charAt(0);
            if (tempOneChart == 0) {
                return null;
            }
            float tempTwoChart = tempStr.charAt(1) - 0x30 - '0';
            float tempThreeChart = tempStr.charAt(2) - 0x20 - '0';
            float tempFour = tempStr.charAt(3) - 0x10 - '0';
            float tempValue = tempTwoChart * 10 + tempThreeChart + (tempFour / 10);
            if (tempOneChart == 43) {
//                temperatureData.setValue(tempValue);
            } else if (tempOneChart == 45) {
                tempValue = tempValue * -1;
//                temperatureData.setValue(-tempValue);
            }
            /*temperatureData.setMacId(homeBean.getDeviceInfo().getId());
            temperatureData.setTime(System.currentTimeMillis());
            DaoHandle.insertRealTimeTemp(temperatureData);
            //解析湿度
            HumidityData humidityData = new HumidityData();*/
            float humidityOneChart = humdityStr.charAt(0) - 0x20 - '0';
            float humidityTwoChar = humdityStr.charAt(1) - 0x10 - '0';
            int humidityValue = (int) (humidityOneChart * 10 + humidityTwoChar);
//            humidityData.setValue(humidityValue);
//            humidityData.setMacId(homeBean.getDeviceInfo().getId());
//            humidityData.setTime(System.currentTimeMillis());
//            DaoHandle.insertRealTimeHumidity(humidityData);
//            //解析气压
//            PressureData pressureData = new PressureData();
            float pressureOneChart = pressureStr.charAt(0) - 0x40 - '0';
            float pressureTwoChart = pressureStr.charAt(1) - 0x30 - '0';
            float pressureThreeChart = pressureStr.charAt(2) - 0x20 - '0';
            float pressureFourChart = pressureStr.charAt(3) - 0x10 - '0';
            int pressureValue = (int) (pressureOneChart * 1000 + pressureTwoChart * 100 + pressureThreeChart * 10 + pressureFourChart);

            realTimeData.setTemp(tempValue);
            realTimeData.setHumidity(humidityValue);
            realTimeData.setPressure(pressureValue);
//            Log.v("RealTimeDataUtils", "tempValue=" + tempValue + " humidityValue=" + humidityValue + " pressureValue=" + pressureValue);
//            pressureData.setValue(pressureValue);
//            pressureData.setMacId(homeBean.getDeviceInfo().getId());
//            pressureData.setTime(System.currentTimeMillis());
//            DaoHandle.insertRealTimePressure(pressureData);
//            homeBean.setTemperatureData(temperatureData);
//            homeBean.setHumidityData(humidityData);
//            homeBean.setPressureData(pressureData);
//            getDataSuccess(homeBean);
//            scanAlarm(deviceInfo, str, homeBean);
            return realTimeData;
        } catch (Exception e) {
            return realTimeData;
        }
    }

    /*
     * 向设备授时CD 55 01 53 06 01 XX 24 38 0C 11 08 0C 2A 40 0B 01 XX CB
     * */
    public byte[] setDeviceTimePack() {
        initData();
        //指令
        datas.add((byte) (Constant.S_LEVEL_IS & 0xFF));
        datas.add((byte) (Constant.set_time_device_id & 0xFF));
        //应答
        datas.add((byte) Constant.REPLY);
        //数据长度
        datas.add((byte) (10 & 0xFF));
        //数据类型及长度
        datas.add((byte) ((4 & 0x1F) << 3 | (4 & 0x07)));
        //数据对应单位以及小数点位数
        datas.add((byte) ((7 & 0x1F) << 3 | (0 & 0x07)));
        LogUtils.e("授时日期对应：", (7 & 0x1F) << 3 | (0 & 0x07));
        //时区
        datas.add(((byte) (TimeZoneUtils.getCurrentTimeZone() & 0xFF)));
        //年
        datas.add((byte) ((TimeZoneUtils.getYear() - 2000) & 0xFF));
        LogUtils.e("年：", TimeZoneUtils.getYear());

        //月
        datas.add((byte) (TimeZoneUtils.getMonth() & 0xFF));
        LogUtils.e("月：", TimeZoneUtils.getMonth());

        //日
        datas.add((byte) (TimeZoneUtils.getDay() & 0xFF));
        LogUtils.e("日：", TimeZoneUtils.getDay());

        datas.add((byte) ((8 & 0x1F) << 3 | (0 & 0x07)));
        //数据对应单位以及小数点位数
        datas.add((byte) ((5 & 0x1F) << 3 | (2 & 0x07)));
        LogUtils.e("授时时分对应：", ((5 & 0x1F) << 3 | (2 & 0x07)));
        //时
        datas.add((byte) (TimeZoneUtils.getHours() & 0xFF));
        LogUtils.e("时：", TimeZoneUtils.getHours());

        //分
        datas.add((byte) (TimeZoneUtils.getMinute() & 0xFF));
        LogUtils.e("分：", TimeZoneUtils.getMinute());
        //校验和
        datas.add(getChecksum(datas));
        datas.add((byte) Constant.TAIL);
        return listTobyte(datas);
    }

    //具有上下限的温湿度警报
    public byte[] setDoubleDeviceAlarm(AlarmData alarmData) {
        initData();
        int type = alarmData.getAlarmType();
        int max = 0;
        int min = 0;
        //指令
        datas.add((byte) (Constant.S_LEVEL_IS & 0xFF));


        if (type == 0) {
            datas.add((byte) (Constant.double_temperature_alarm & 0xFF));
            //应答
            datas.add((byte) Constant.REPLY);
            //数据长度
            datas.add((byte) (8 & 0xFF));
            //数据包类型及长度
            datas.add((byte) ((0 & 0x1F) << 3 | (2 & 0x07)));
            //数据对应单位以及小数点位数
            datas.add((byte) ((0 & 0x1F) << 3 | (1 & 0x07)));
            max = alarmData.getMaxTemperatureAlarmValue();
            min = alarmData.getMinTemperatureAlarmValue();
            if (max < 0) {
                byte[] maxByte = ConvertUtils.minusIntTo2Bytes(max * 10);
                datas.add(maxByte[1]);
                datas.add(maxByte[0]);
            } else {
                byte[] maxByte = ConvertUtils.intTo2Bytes(max * 10);
                datas.add(maxByte[1]);
                datas.add(maxByte[0]);
            }

            //数据包类型及长度
            datas.add((byte) ((0 & 0x1F) << 3 | (2 & 0x07)));
            //数据对应单位以及小数点位数
            datas.add((byte) ((0 & 0x1F) << 3 | (1 & 0x07)));
            if (min < 0) {
                byte[] minByte = ConvertUtils.minusIntTo2Bytes(Math.abs(min) * 10);
                datas.add(minByte[1]);
                datas.add(minByte[0]);
            } else {
                byte[] minByte = ConvertUtils.intTo2Bytes(min * 10);
                datas.add(minByte[1]);
                datas.add(minByte[0]);
            }
        } else if (type == 1) {
            datas.add((byte) (Constant.double_humidity_alarm & 0xFF));
            //应答
            datas.add((byte) Constant.REPLY);
            //数据长度
            datas.add((byte) (6 & 0xFF));
            //数据包类型及长度
            datas.add((byte) ((1 & 0x1F) << 3 | (1 & 0x07)));
            //数据对应单位以及小数点位数
            datas.add((byte) ((9 & 0x1F) << 3 | (0 & 0x07)));
            max = alarmData.getMaxhumidityAlarmValue();
            min = alarmData.getMinhumidityAlarmValue();
            datas.add((byte) (max & 0xFF));
            LogUtils.e("湿度警：", max & 0xFF);
            //数据包类型及长度
            datas.add((byte) ((1 & 0x1F) << 3 | (1 & 0x07)));
            //数据对应单位以及小数点位数
            datas.add((byte) ((9 & 0x1F) << 3 | (0 & 0x07)));
            datas.add((byte) (min & 0xFF));
            LogUtils.e("湿度小警报取后8位：", min & 0xFF);
        }

        //校验和
        datas.add(getChecksum(datas));
        datas.add((byte) Constant.TAIL);
        return listTobyte(datas);
    }

    //具有上下限的温湿度警报
    public byte[] setDoubleDeviceAlarm(int type, int max, int min) {
        initData();
        if (max < min) {
            int a = min;
            min = max;
            max = a;
        }
//        int type = alarmData.getAlarmType();
//        int max = 0;
//        int min = 0;
        //指令
        datas.add((byte) (Constant.S_LEVEL_IS & 0xFF));

        if (type == 0) {
            datas.add((byte) (Constant.double_temperature_alarm & 0xFF));
            //应答
            datas.add((byte) Constant.REPLY);
            //数据长度
            datas.add((byte) (8 & 0xFF));
            //数据包类型及长度
            datas.add((byte) ((0 & 0x1F) << 3 | (2 & 0x07)));
            //数据对应单位以及小数点位数
            datas.add((byte) ((0 & 0x1F) << 3 | (1 & 0x07)));
//            max = alarmData.getMaxTemperatureAlarmValue();
//            min = alarmData.getMinTemperatureAlarmValue();
            if (max < 0) {
                byte[] maxByte = ConvertUtils.minusIntTo2Bytes(max * 10);
                datas.add(maxByte[1]);
                datas.add(maxByte[0]);
            } else {
                byte[] maxByte = ConvertUtils.intTo2Bytes(max * 10);
                datas.add(maxByte[1]);
                datas.add(maxByte[0]);
            }

            //数据包类型及长度
            datas.add((byte) ((0 & 0x1F) << 3 | (2 & 0x07)));
            //数据对应单位以及小数点位数
            datas.add((byte) ((0 & 0x1F) << 3 | (1 & 0x07)));
            if (min < 0) {
                byte[] minByte = ConvertUtils.minusIntTo2Bytes(Math.abs(min) * 10);
                datas.add(minByte[1]);
                datas.add(minByte[0]);
            } else {
                byte[] minByte = ConvertUtils.intTo2Bytes(min * 10);
                datas.add(minByte[1]);
                datas.add(minByte[0]);
            }
        } else {
            datas.add((byte) (Constant.double_humidity_alarm & 0xFF));
            //应答
            datas.add((byte) Constant.REPLY);
            //数据长度
            datas.add((byte) (6 & 0xFF));
            //数据包类型及长度
            datas.add((byte) ((1 & 0x1F) << 3 | (1 & 0x07)));
            //数据对应单位以及小数点位数
            datas.add((byte) ((9 & 0x1F) << 3 | (0 & 0x07)));
//            max = alarmData.getMaxhumidityAlarmValue();
//            min = alarmData.getMinhumidityAlarmValue();
            datas.add((byte) (max & 0xFF));
            LogUtils.e("湿度警：", max & 0xFF);
            //数据包类型及长度
            datas.add((byte) ((1 & 0x1F) << 3 | (1 & 0x07)));
            //数据对应单位以及小数点位数
            datas.add((byte) ((9 & 0x1F) << 3 | (0 & 0x07)));
            datas.add((byte) (min & 0xFF));
            LogUtils.e("湿度小警报取后8位：", min & 0xFF);
        }

        //校验和
        datas.add(getChecksum(datas));
        datas.add((byte) Constant.TAIL);
        return listTobyte(datas);
    }


    public byte[] listTobyte(ArrayList dataArr) {
        Object[] b = (Object[]) dataArr.toArray();
        byte[] arr = new byte[b.length];
        for (int i = 0; i < b.length; i++) {
            arr[i] = (Byte) b[i];
        }
        final StringBuilder stringBuilder = new StringBuilder(arr.length);
        for (byte byteChar : arr)
            //以十六进制的形式输出
            stringBuilder.append(String.format("%02X ", byteChar));
        // intent.putExtra(EXTRA_DATA, new String(data) + "\n" + stringBuilder.toString());
        LogUtils.e("当前包的数据：", stringBuilder.toString());
        if (datas != null) {
//            LogUtils.e("发包校验和结果：", check(arr));
            datas.clear();
        }
        return arr;
    }

    public int generateShortUuid() {
        StringBuilder shortBuffer = new StringBuilder();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        int chat = 0;
        for (int i = 0; i < 8; i++) {
            String str = uuid.substring(i * 4, i * 4 + 4);
            int x = Integer.parseInt(str, 16);
            chat = Integer.parseInt(Integer.toOctalString(GetCharAscii.SumStrAscii(chars[x % 0x3E]))) >> 4;
            if (shortBuffer.length() < 7) {
                shortBuffer.append(chat);
            }
        }
        LogUtils.d("当前随机uuid:", shortBuffer.toString());
        int value = Integer.parseInt(shortBuffer.toString());
        return value;
    }

    public static int getHeight(byte data) {//获取高5位
        int height;
        height = (data & 0xf8) >> 3;
        return height;
    }

    public static int getLow(byte data) {//获取低3位
        int low;
        low = (data & 0x07);
        return low;
    }

    //校验和
    public static byte getChecksum(ArrayList<Byte> datas) {
        byte sum = 0;
        for (int i = 0; i < datas.size(); i++) {
            sum += datas.get(i);
        }
//        LogUtils.e("当前校验和:", sum);
        return (byte) (sum & 0xFF);
    }

    //校验和
    public byte getChecksum(byte[] datas) {
        byte sum = 0;
        for (int i = 0; i < datas.length - 2; i++) {
            sum += datas[i];
        }
//        LogUtils.e("当前校验和:", sum);
        return (byte) (sum & 0xFF);
    }

    /*日期*/
    //
    public static void getDate(byte time, int index, TimeData timeData) {
        switch (index) {
            case 0:
                LogUtils.e("时区：", ConvertUtils.byteToInt(time));
                timeData.setTimeZone(ConvertUtils.byteToInt(time));
                break;
            case 1:
                LogUtils.e("年：", ConvertUtils.byteToInt(time));
                timeData.setYear(ConvertUtils.byteToInt(time));
                break;
            case 2:
                LogUtils.e("月：", ConvertUtils.byteToInt(time));
                timeData.setMonth(ConvertUtils.byteToInt(time));
                break;
            case 3:
                LogUtils.e("日：", ConvertUtils.byteToInt(time));
                timeData.setDay(ConvertUtils.byteToInt(time));
                break;
        }
    }

    /*时间*/
    public static void getTimeData(byte time, int index, TimeData timeData) {
        switch (index) {
            case 0:
                LogUtils.e("时", ConvertUtils.byteToInt(time));
                timeData.setHours(ConvertUtils.byteToInt(time));
                break;
            case 1:
                LogUtils.e("分：", ConvertUtils.byteToInt(time));
                timeData.setMinute(ConvertUtils.byteToInt(time));
                break;
            case 2:
                LogUtils.e("秒：", ConvertUtils.byteToInt(time));
                timeData.setSecond(ConvertUtils.byteToInt(time));
                break;
        }
    }

    public Boolean check(byte[] data) {
        byte checkData = (byte) (data[data.length - 2] & 0xFF);
        byte sum = getChecksum(data);
        return checkData == sum;
    }

    private int i = 0;

    /* 打印* */
    public void logPrint(byte[] data) {
        final StringBuilder stringBuilder = new StringBuilder(data.length);
        for (byte byteChar : data) {
            //以十六进制的形式输出
            stringBuilder.append(String.format("%02X ", byteChar));
        }
        i++;
        // intent.putExtra(EXTRA_DATA, new String(data) + "\n" + stringBuilder.toString());
        LogUtils.e("蓝牙返回的第+" + i + "数据：", stringBuilder.toString());
    }

}
