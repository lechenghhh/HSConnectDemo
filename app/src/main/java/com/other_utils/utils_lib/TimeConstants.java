package com.other_utils.utils_lib;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by 90835 on 2017/4/17.
 */

public class TimeConstants {
    /**
     * 秒与毫秒的倍数
     */
    public static final int MSEC = 1;
    /**
     * 秒与毫秒的倍数
     */
    public static final int SEC  = 1000;
    /**
     * 分与毫秒的倍数
     */
    public static final int MIN  = 60000;
    /**
     * 时与毫秒的倍数
     */
    public static final int HOUR = 3600000;
    /**
     * 天与毫秒的倍数
     */
    public static final int DAY  = 86400000;

    /**
     * 年与毫秒的倍数
     */
    public static final int YEAR  = DAY*365;

    @IntDef({MSEC, SEC, MIN, HOUR, DAY,YEAR})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Unit {
    }
}
