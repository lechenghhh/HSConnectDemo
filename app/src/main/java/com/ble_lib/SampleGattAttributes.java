/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ble_lib;

import java.util.HashMap;

public class SampleGattAttributes {
    private static HashMap<String, String> attributes = new HashMap();
    //版本
    public static String HEART_RATE_MEASUREMENT = "00002902-0000-1000-8000-00805f9b34fb";
    public static String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";
    public static String UUID_MEASUREMENT = "00001801-0000-1000-8000-00805F9B34FB";
    public static String UUID_CHECK = "6E400002-B5A3-F393-E0A9-E50E24DCCA9E";
    public static String UUID_SETTING = "0000fff0-0000-1000-8000-00805f9b34fb";
    public static String UUID_NOTIFY = "0000fff1-0000-1000-8000-00805f9b34fb";
    public static String UUID_WRITE = "0000fff3-0000-1000-8000-00805f9b34fb";


    static {
        // GATT Services
        attributes.put("00001800-0000-1000-8000-00805f9b34fb", "GenericAccess");
        attributes.put("00001801-0000-1000-8000-00805f9b34fb", "GenericAttribute");
        attributes.put(UUID_SETTING, "通道设置");
        // GATT Characteristics
        attributes.put("00002a00-0000-1000-8000-00805f9b34fb", "Device Name");
        attributes.put("00002a01-0000-1000-8000-00805f9b34fb", "Appearance");
        attributes.put("00002a02-0000-1000-8000-00805f9b34fb", "Peripheral Privacy Flag");
        attributes.put("00002a03-0000-1000-8000-00805f9b34fb", "Reconnection Address");
        attributes.put("00002a04-0000-1000-8000-00805f9b34fb", "PPCP");
        attributes.put("00002a05-0000-1000-8000-00805f9b34fb", "Service Changed");
        attributes.put("UUID_SETTING", "通道设置");


        // Characteristic UUIDs
        attributes.put(HEART_RATE_MEASUREMENT, "Heart Rate Measurement");
        attributes.put(UUID_MEASUREMENT, "SOME FIND");
        attributes.put("00002a29-0000-1000-8000-00805f9b34fb", "Manufacturer Name String");
        attributes.put(UUID_WRITE, "Home Smart WRITE");//某个可写设备的写特征
        attributes.put(UUID_NOTIFY, "Home Smart Notify");//某个设备的Notify特征
        attributes.put("2A28", "software version"); // 固件版本号
        attributes.put("2A23", "MAC地址"); //MAC 地址
        attributes.put("2A25", "IMEI"); //IMEI 号

    }

    public static String lookup(String uuid) {
        String low = attributes.get(uuid.toLowerCase());
        String up = attributes.get(uuid.toUpperCase());
        String name = low == null ? (up == null ? null : up) : low;
        return name;
    }
}
