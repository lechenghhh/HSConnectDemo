package com.ble_lib.exception;



public class NoSupportBleException extends Exception {
    public NoSupportBleException() {
        super("The device is not support ble");
    }
}
