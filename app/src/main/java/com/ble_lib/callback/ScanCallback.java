package com.ble_lib.callback;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanRecord;


public interface ScanCallback {
      void onDeviceFound(BluetoothDevice devices, ScanRecord scanRecord);

      void onFoundFail();
}
